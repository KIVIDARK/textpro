﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextPro
{
	public partial class AddDateForm : Form
	{
		public AddDateForm()
		{
			InitializeComponent();
		}

		String temp = String.Empty;

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		public string GetString()
		{
			return temp;
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			temp = listBox1.SelectedItem.ToString();
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}

		private void AddDateForm_Shown(object sender, EventArgs e)
		{
			listBox1.Items.Add(DateTime.Now.ToString());
			listBox1.Items.Add(DateTime.Now.ToLongDateString());
			listBox1.Items.Add(DateTime.Now.ToShortDateString());
			listBox1.Items.Add(DateTime.Now.ToLongTimeString());
			listBox1.Items.Add(DateTime.Now.ToShortTimeString());
		}
	}
}
