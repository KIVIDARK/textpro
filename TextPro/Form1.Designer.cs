﻿namespace TextPro
{
	partial class Form1
	{
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.сохранитькакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.отменадействияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.выделитьвсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.шрифтToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сервисToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.параметрыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.содержаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.индексToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.поискToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.опрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.counterL = new System.Windows.Forms.ToolStripStatusLabel();
			this.pagePanel = new System.Windows.Forms.Panel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxStrikeout = new System.Windows.Forms.CheckBox();
			this.checkBoxUnderlined = new System.Windows.Forms.CheckBox();
			this.checkBoxItalic = new System.Windows.Forms.CheckBox();
			this.checkBoxBold = new System.Windows.Forms.CheckBox();
			this.buttonBackrgoundColor = new System.Windows.Forms.Button();
			this.buttonTextColor = new System.Windows.Forms.Button();
			this.numericUpDownFontSize = new System.Windows.Forms.NumericUpDown();
			this.comboBoxFonts = new System.Windows.Forms.ComboBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.colorDialogFont = new System.Windows.Forms.ColorDialog();
			this.colorDialogBack = new System.Windows.Forms.ColorDialog();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.buttonPasteDate = new System.Windows.Forms.Button();
			this.buttonPasteImage = new System.Windows.Forms.Button();
			this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
			this.buttonPaste = new System.Windows.Forms.Button();
			this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.печатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.предварительныйпросмотрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.вырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.вставкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.pagePanel.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownFontSize)).BeginInit();
			this.tabPage2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel5.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBox1
			// 
			this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.richTextBox1.EnableAutoDragDrop = true;
			this.richTextBox1.Location = new System.Drawing.Point(30, 30);
			this.richTextBox1.Margin = new System.Windows.Forms.Padding(0);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.richTextBox1.ShowSelectionMargin = true;
			this.richTextBox1.Size = new System.Drawing.Size(410, 350);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = "";
			this.richTextBox1.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.richTextBox1_ContentsResized);
			this.richTextBox1.SelectionChanged += new System.EventHandler(this.richTextBox1_SelectionChanged);
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.правкаToolStripMenuItem,
            this.сервисToolStripMenuItem,
            this.справкаToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(854, 24);
			this.menuStrip1.TabIndex = 2;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// файлToolStripMenuItem
			// 
			this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.открытьToolStripMenuItem,
            this.toolStripSeparator,
            this.сохранитьToolStripMenuItem,
            this.сохранитькакToolStripMenuItem,
            this.toolStripSeparator1,
            this.печатьToolStripMenuItem,
            this.предварительныйпросмотрToolStripMenuItem,
            this.toolStripSeparator2,
            this.выходToolStripMenuItem});
			this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
			this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.файлToolStripMenuItem.Text = "&Файл";
			// 
			// toolStripSeparator
			// 
			this.toolStripSeparator.Name = "toolStripSeparator";
			this.toolStripSeparator.Size = new System.Drawing.Size(230, 6);
			// 
			// сохранитькакToolStripMenuItem
			// 
			this.сохранитькакToolStripMenuItem.Name = "сохранитькакToolStripMenuItem";
			this.сохранитькакToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.сохранитькакToolStripMenuItem.Text = "Сохранить &как";
			this.сохранитькакToolStripMenuItem.Click += new System.EventHandler(this.сохранитькакToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(230, 6);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(230, 6);
			// 
			// выходToolStripMenuItem
			// 
			this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
			this.выходToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.выходToolStripMenuItem.Text = "Вы&ход";
			this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
			// 
			// правкаToolStripMenuItem
			// 
			this.правкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отменадействияToolStripMenuItem,
            this.toolStripSeparator3,
            this.вырезатьToolStripMenuItem,
            this.копироватьToolStripMenuItem,
            this.вставкаToolStripMenuItem,
            this.toolStripSeparator4,
            this.выделитьвсеToolStripMenuItem,
            this.шрифтToolStripMenuItem});
			this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
			this.правкаToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
			this.правкаToolStripMenuItem.Text = "&Правка";
			// 
			// отменадействияToolStripMenuItem
			// 
			this.отменадействияToolStripMenuItem.Name = "отменадействияToolStripMenuItem";
			this.отменадействияToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
			this.отменадействияToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.отменадействияToolStripMenuItem.Text = "&Отмена действия";
			this.отменадействияToolStripMenuItem.Click += new System.EventHandler(this.отменадействияToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(206, 6);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(206, 6);
			// 
			// выделитьвсеToolStripMenuItem
			// 
			this.выделитьвсеToolStripMenuItem.Name = "выделитьвсеToolStripMenuItem";
			this.выделитьвсеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
			this.выделитьвсеToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.выделитьвсеToolStripMenuItem.Text = "Выделить &все";
			this.выделитьвсеToolStripMenuItem.Click += new System.EventHandler(this.выделитьвсеToolStripMenuItem_Click);
			// 
			// шрифтToolStripMenuItem
			// 
			this.шрифтToolStripMenuItem.Name = "шрифтToolStripMenuItem";
			this.шрифтToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.шрифтToolStripMenuItem.Text = "Шрифт";
			this.шрифтToolStripMenuItem.Click += new System.EventHandler(this.шрифтToolStripMenuItem_Click);
			// 
			// сервисToolStripMenuItem
			// 
			this.сервисToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem,
            this.параметрыToolStripMenuItem});
			this.сервисToolStripMenuItem.Name = "сервисToolStripMenuItem";
			this.сервисToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
			this.сервисToolStripMenuItem.Text = "&Сервис";
			// 
			// настройкиToolStripMenuItem
			// 
			this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
			this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.настройкиToolStripMenuItem.Text = "&Настройки";
			// 
			// параметрыToolStripMenuItem
			// 
			this.параметрыToolStripMenuItem.Name = "параметрыToolStripMenuItem";
			this.параметрыToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.параметрыToolStripMenuItem.Text = "&Параметры";
			// 
			// справкаToolStripMenuItem
			// 
			this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.содержаниеToolStripMenuItem,
            this.индексToolStripMenuItem,
            this.поискToolStripMenuItem,
            this.toolStripSeparator5,
            this.опрограммеToolStripMenuItem});
			this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
			this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
			this.справкаToolStripMenuItem.Text = "Спра&вка";
			// 
			// содержаниеToolStripMenuItem
			// 
			this.содержаниеToolStripMenuItem.Name = "содержаниеToolStripMenuItem";
			this.содержаниеToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.содержаниеToolStripMenuItem.Text = "&Содержание";
			// 
			// индексToolStripMenuItem
			// 
			this.индексToolStripMenuItem.Name = "индексToolStripMenuItem";
			this.индексToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.индексToolStripMenuItem.Text = "&Индекс";
			// 
			// поискToolStripMenuItem
			// 
			this.поискToolStripMenuItem.Name = "поискToolStripMenuItem";
			this.поискToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.поискToolStripMenuItem.Text = "&Поиск";
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(155, 6);
			// 
			// опрограммеToolStripMenuItem
			// 
			this.опрограммеToolStripMenuItem.Name = "опрограммеToolStripMenuItem";
			this.опрограммеToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.опрограммеToolStripMenuItem.Text = "&О программе...";
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.counterL});
			this.statusStrip1.Location = new System.Drawing.Point(0, 480);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(854, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// counterL
			// 
			this.counterL.Name = "counterL";
			this.counterL.Size = new System.Drawing.Size(145, 17);
			this.counterL.Text = "Количество символов : 0";
			// 
			// pagePanel
			// 
			this.pagePanel.BackColor = System.Drawing.Color.White;
			this.pagePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pagePanel.Controls.Add(this.richTextBox1);
			this.pagePanel.Location = new System.Drawing.Point(0, 0);
			this.pagePanel.Margin = new System.Windows.Forms.Padding(0);
			this.pagePanel.Name = "pagePanel";
			this.pagePanel.Size = new System.Drawing.Size(200, 375);
			this.pagePanel.TabIndex = 6;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.vScrollBar1, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 120);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 346F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(854, 336);
			this.tableLayoutPanel1.TabIndex = 8;
			this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(831, 330);
			this.tableLayoutPanel2.TabIndex = 8;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.pagePanel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(315, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(200, 330);
			this.panel1.TabIndex = 7;
			// 
			// vScrollBar1
			// 
			this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.vScrollBar1.LargeChange = 50;
			this.vScrollBar1.Location = new System.Drawing.Point(837, 0);
			this.vScrollBar1.Name = "vScrollBar1";
			this.vScrollBar1.Size = new System.Drawing.Size(17, 336);
			this.vScrollBar1.SmallChange = 20;
			this.vScrollBar1.TabIndex = 9;
			this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
			this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
			// 
			// fontDialog1
			// 
			this.fontDialog1.Color = System.Drawing.SystemColors.ControlText;
			this.fontDialog1.MaxSize = 108;
			this.fontDialog1.MinSize = 4;
			this.fontDialog1.ShowColor = true;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "Новый документ";
			this.openFileDialog1.Filter = "Текстовый файл rtf | *.rtf";
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.FileName = "Новый документ";
			this.saveFileDialog1.Filter = "Текстовый файл rtf | *.rtf";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(3, 3);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(848, 114);
			this.tabControl1.TabIndex = 9;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.tableLayoutPanel4);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(840, 88);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Форматирование";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 3;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 230F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 1;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(840, 88);
			this.tableLayoutPanel4.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxStrikeout);
			this.groupBox1.Controls.Add(this.checkBoxUnderlined);
			this.groupBox1.Controls.Add(this.checkBoxItalic);
			this.groupBox1.Controls.Add(this.checkBoxBold);
			this.groupBox1.Controls.Add(this.buttonBackrgoundColor);
			this.groupBox1.Controls.Add(this.buttonTextColor);
			this.groupBox1.Controls.Add(this.numericUpDownFontSize);
			this.groupBox1.Controls.Add(this.comboBoxFonts);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(224, 82);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Шрифт";
			// 
			// checkBoxStrikeout
			// 
			this.checkBoxStrikeout.Appearance = System.Windows.Forms.Appearance.Button;
			this.checkBoxStrikeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxStrikeout.Location = new System.Drawing.Point(96, 52);
			this.checkBoxStrikeout.Name = "checkBoxStrikeout";
			this.checkBoxStrikeout.Size = new System.Drawing.Size(24, 24);
			this.checkBoxStrikeout.TabIndex = 6;
			this.checkBoxStrikeout.Text = "S";
			this.checkBoxStrikeout.UseVisualStyleBackColor = true;
			this.checkBoxStrikeout.CheckedChanged += new System.EventHandler(this.checkBoxStrikeout_CheckedChanged);
			// 
			// checkBoxUnderlined
			// 
			this.checkBoxUnderlined.Appearance = System.Windows.Forms.Appearance.Button;
			this.checkBoxUnderlined.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxUnderlined.Location = new System.Drawing.Point(66, 52);
			this.checkBoxUnderlined.Name = "checkBoxUnderlined";
			this.checkBoxUnderlined.Size = new System.Drawing.Size(24, 24);
			this.checkBoxUnderlined.TabIndex = 5;
			this.checkBoxUnderlined.Text = "U";
			this.checkBoxUnderlined.UseVisualStyleBackColor = true;
			this.checkBoxUnderlined.CheckedChanged += new System.EventHandler(this.checkBoxUnderlined_CheckedChanged);
			// 
			// checkBoxItalic
			// 
			this.checkBoxItalic.Appearance = System.Windows.Forms.Appearance.Button;
			this.checkBoxItalic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxItalic.Location = new System.Drawing.Point(36, 52);
			this.checkBoxItalic.Name = "checkBoxItalic";
			this.checkBoxItalic.Size = new System.Drawing.Size(24, 24);
			this.checkBoxItalic.TabIndex = 4;
			this.checkBoxItalic.Text = "I";
			this.checkBoxItalic.UseVisualStyleBackColor = true;
			this.checkBoxItalic.CheckedChanged += new System.EventHandler(this.checkBoxItalic_CheckedChanged);
			// 
			// checkBoxBold
			// 
			this.checkBoxBold.Appearance = System.Windows.Forms.Appearance.Button;
			this.checkBoxBold.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.checkBoxBold.Location = new System.Drawing.Point(6, 52);
			this.checkBoxBold.Name = "checkBoxBold";
			this.checkBoxBold.Size = new System.Drawing.Size(24, 24);
			this.checkBoxBold.TabIndex = 3;
			this.checkBoxBold.Text = "B";
			this.checkBoxBold.UseVisualStyleBackColor = true;
			this.checkBoxBold.CheckedChanged += new System.EventHandler(this.checkBoxBold_CheckedChanged);
			// 
			// buttonBackrgoundColor
			// 
			this.buttonBackrgoundColor.BackColor = System.Drawing.Color.White;
			this.buttonBackrgoundColor.Location = new System.Drawing.Point(179, 52);
			this.buttonBackrgoundColor.Name = "buttonBackrgoundColor";
			this.buttonBackrgoundColor.Size = new System.Drawing.Size(33, 24);
			this.buttonBackrgoundColor.TabIndex = 2;
			this.toolTip1.SetToolTip(this.buttonBackrgoundColor, "Цвет заливки текста");
			this.buttonBackrgoundColor.UseVisualStyleBackColor = false;
			this.buttonBackrgoundColor.Click += new System.EventHandler(this.buttonBackrgoundColor_Click);
			// 
			// buttonTextColor
			// 
			this.buttonTextColor.BackColor = System.Drawing.Color.Black;
			this.buttonTextColor.Location = new System.Drawing.Point(129, 52);
			this.buttonTextColor.Name = "buttonTextColor";
			this.buttonTextColor.Size = new System.Drawing.Size(31, 24);
			this.buttonTextColor.TabIndex = 2;
			this.toolTip1.SetToolTip(this.buttonTextColor, "Цвет текста");
			this.buttonTextColor.UseVisualStyleBackColor = false;
			this.buttonTextColor.Click += new System.EventHandler(this.buttonTextColor_Click);
			// 
			// numericUpDownFontSize
			// 
			this.numericUpDownFontSize.Location = new System.Drawing.Point(129, 19);
			this.numericUpDownFontSize.Maximum = new decimal(new int[] {
            108,
            0,
            0,
            0});
			this.numericUpDownFontSize.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
			this.numericUpDownFontSize.Name = "numericUpDownFontSize";
			this.numericUpDownFontSize.Size = new System.Drawing.Size(83, 20);
			this.numericUpDownFontSize.TabIndex = 1;
			this.numericUpDownFontSize.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
			this.numericUpDownFontSize.ValueChanged += new System.EventHandler(this.numericUpDownFontSize_ValueChanged);
			// 
			// comboBoxFonts
			// 
			this.comboBoxFonts.FormattingEnabled = true;
			this.comboBoxFonts.Location = new System.Drawing.Point(6, 19);
			this.comboBoxFonts.Name = "comboBoxFonts";
			this.comboBoxFonts.Size = new System.Drawing.Size(117, 21);
			this.comboBoxFonts.TabIndex = 0;
			this.comboBoxFonts.SelectedIndexChanged += new System.EventHandler(this.comboBoxFonts_SelectedIndexChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.tableLayoutPanel5);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(840, 88);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Контент";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.tabControl1, 0, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 24);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(854, 456);
			this.tableLayoutPanel3.TabIndex = 9;
			// 
			// colorDialogFont
			// 
			this.colorDialogFont.FullOpen = true;
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.ColumnCount = 2;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Controls.Add(this.groupBox2, 0, 0);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.RowCount = 1;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(834, 82);
			this.tableLayoutPanel5.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.buttonPasteImage);
			this.groupBox2.Controls.Add(this.buttonPasteDate);
			this.groupBox2.Controls.Add(this.buttonPaste);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(3, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(194, 76);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Вставка";
			// 
			// buttonPasteDate
			// 
			this.buttonPasteDate.Location = new System.Drawing.Point(62, 47);
			this.buttonPasteDate.Name = "buttonPasteDate";
			this.buttonPasteDate.Size = new System.Drawing.Size(124, 23);
			this.buttonPasteDate.TabIndex = 1;
			this.buttonPasteDate.Text = "Вставить Дату";
			this.buttonPasteDate.UseVisualStyleBackColor = true;
			this.buttonPasteDate.Click += new System.EventHandler(this.buttonPasteDate_Click);
			// 
			// buttonPasteImage
			// 
			this.buttonPasteImage.Location = new System.Drawing.Point(62, 18);
			this.buttonPasteImage.Name = "buttonPasteImage";
			this.buttonPasteImage.Size = new System.Drawing.Size(124, 23);
			this.buttonPasteImage.TabIndex = 2;
			this.buttonPasteImage.Text = "Вставить картинку";
			this.buttonPasteImage.UseVisualStyleBackColor = true;
			this.buttonPasteImage.Click += new System.EventHandler(this.buttonPasteImage_Click);
			// 
			// openFileDialogImage
			// 
			this.openFileDialogImage.FileName = "Image";
			this.openFileDialogImage.Filter = "JPEG|*.jpg|PNG|*.png|BMP|*.bmp|Изображения|*.jpg;*.png;*.bmp";
			this.openFileDialogImage.FilterIndex = 4;
			this.openFileDialogImage.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialogImage_FileOk);
			// 
			// buttonPaste
			// 
			this.buttonPaste.BackgroundImage = global::TextPro.Properties.Resources.edit_paste_3286;
			this.buttonPaste.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.buttonPaste.Location = new System.Drawing.Point(6, 19);
			this.buttonPaste.Name = "buttonPaste";
			this.buttonPaste.Size = new System.Drawing.Size(50, 50);
			this.buttonPaste.TabIndex = 0;
			this.buttonPaste.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.buttonPaste.UseVisualStyleBackColor = true;
			this.buttonPaste.Click += new System.EventHandler(this.buttonPaste_Click);
			// 
			// создатьToolStripMenuItem
			// 
			this.создатьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("создатьToolStripMenuItem.Image")));
			this.создатьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
			this.создатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.создатьToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.создатьToolStripMenuItem.Text = "&Создать";
			this.создатьToolStripMenuItem.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
			// 
			// открытьToolStripMenuItem
			// 
			this.открытьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("открытьToolStripMenuItem.Image")));
			this.открытьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
			this.открытьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.открытьToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.открытьToolStripMenuItem.Text = "&Открыть";
			this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
			// 
			// сохранитьToolStripMenuItem
			// 
			this.сохранитьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("сохранитьToolStripMenuItem.Image")));
			this.сохранитьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
			this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.сохранитьToolStripMenuItem.Text = "&Сохранить";
			this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
			// 
			// печатьToolStripMenuItem
			// 
			this.печатьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("печатьToolStripMenuItem.Image")));
			this.печатьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.печатьToolStripMenuItem.Name = "печатьToolStripMenuItem";
			this.печатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
			this.печатьToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.печатьToolStripMenuItem.Text = "&Печать";
			// 
			// предварительныйпросмотрToolStripMenuItem
			// 
			this.предварительныйпросмотрToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("предварительныйпросмотрToolStripMenuItem.Image")));
			this.предварительныйпросмотрToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.предварительныйпросмотрToolStripMenuItem.Name = "предварительныйпросмотрToolStripMenuItem";
			this.предварительныйпросмотрToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.предварительныйпросмотрToolStripMenuItem.Text = "Предварительный про&смотр";
			// 
			// вырезатьToolStripMenuItem
			// 
			this.вырезатьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("вырезатьToolStripMenuItem.Image")));
			this.вырезатьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.вырезатьToolStripMenuItem.Name = "вырезатьToolStripMenuItem";
			this.вырезатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
			this.вырезатьToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.вырезатьToolStripMenuItem.Text = "Вырезат&ь";
			this.вырезатьToolStripMenuItem.Click += new System.EventHandler(this.вырезатьToolStripMenuItem_Click);
			// 
			// копироватьToolStripMenuItem
			// 
			this.копироватьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("копироватьToolStripMenuItem.Image")));
			this.копироватьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
			this.копироватьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
			this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.копироватьToolStripMenuItem.Text = "&Копировать";
			this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.копироватьToolStripMenuItem_Click);
			// 
			// вставкаToolStripMenuItem
			// 
			this.вставкаToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("вставкаToolStripMenuItem.Image")));
			this.вставкаToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.вставкаToolStripMenuItem.Name = "вставкаToolStripMenuItem";
			this.вставкаToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
			this.вставкаToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.вставкаToolStripMenuItem.Text = "Вст&авка";
			this.вставкаToolStripMenuItem.Click += new System.EventHandler(this.вставкаToolStripMenuItem_Click);
			// 
			// Form1
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(854, 502);
			this.Controls.Add(this.tableLayoutPanel3);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(600, 540);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TextPro";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Shown += new System.EventHandler(this.Form1_Shown);
			this.Resize += new System.EventHandler(this.Form1_Resize);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.pagePanel.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tableLayoutPanel4.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownFontSize)).EndInit();
			this.tabPage2.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel5.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem создатьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
		private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сохранитькакToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem печатьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem предварительныйпросмотрToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem отменадействияToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem вставкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem выделитьвсеToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сервисToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem параметрыToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem содержаниеToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem индексToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem поискToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem опрограммеToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Panel pagePanel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.ToolStripStatusLabel counterL;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ToolStripMenuItem шрифтToolStripMenuItem;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.VScrollBar vScrollBar1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox comboBoxFonts;
		private System.Windows.Forms.NumericUpDown numericUpDownFontSize;
		private System.Windows.Forms.Button buttonBackrgoundColor;
		private System.Windows.Forms.Button buttonTextColor;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.CheckBox checkBoxBold;
		private System.Windows.Forms.CheckBox checkBoxStrikeout;
		private System.Windows.Forms.CheckBox checkBoxUnderlined;
		private System.Windows.Forms.CheckBox checkBoxItalic;
		private System.Windows.Forms.ColorDialog colorDialogFont;
		private System.Windows.Forms.ColorDialog colorDialogBack;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button buttonPaste;
		private System.Windows.Forms.Button buttonPasteImage;
		private System.Windows.Forms.Button buttonPasteDate;
		private System.Windows.Forms.OpenFileDialog openFileDialogImage;
	}
}

