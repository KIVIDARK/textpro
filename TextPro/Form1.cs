﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextPro
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			richTextBox1.MouseWheel += new MouseEventHandler(Panel_MouseWheel);
		}

		void Panel_MouseWheel(object sender, MouseEventArgs e)
		{
			//throw new NotImplementedException();
			if (vScrollBar1.Value - e.Delta < 0)
			{
				vScrollBar1.Value = 0;
			}
			else
			if (vScrollBar1.Value - e.Delta > vScrollBar1.Maximum)
			{
				vScrollBar1.Value = vScrollBar1.Maximum;
			}
			else
			{
				vScrollBar1.Value -= e.Delta;
			}
		}

		//bool isNormalContent = true;
		bool isFileSave = true;

		int borderLeft = 30;
		int borderRight = 30;
		int borderTop = 30;
		int borderBottom = 40;

		float resX = 0.0f;
		float resY = 0.0f;
		float scale = 1.0f;

		int A4Height = 0;

		int rtbPosX = 0;
		int rtbPosY = 0;

		String currentFileName = String.Empty;

		//List<List<string>> DocText = new List<List<string>>();

		//RichTextBox tempRTB = new RichTextBox();

		private void RTBposition()
		{
			Point loc = new Point(rtbPosX, -rtbPosY);
			pagePanel.Location = loc;
		}

		private void RTBResize()
		{
			vScrollBar1.Maximum = pagePanel.Height + vScrollBar1.LargeChange;
			richTextBox1.Width = pagePanel.Width - (borderLeft + borderRight);
			richTextBox1.Height = pagePanel.Height - (borderTop + borderBottom);
		}

		private void RTBScale()
		{
			SizeF sizef = new SizeF(scale, scale);
			richTextBox1.Scale(sizef);
			richTextBox1.ZoomFactor = scale;

			
		}

		private void Form1_Shown(object sender, EventArgs e)
		{
			RTBScale();
			Graphics g = this.CreateGraphics();
			try
			{
				resX = g.DpiX;
				resY = g.DpiY;
			}
			finally
			{
				g.Dispose();
			}
			panel1.Width = (int)Math.Round(8.27f * resX);
			pagePanel.Width = (int)Math.Round(8.27f * resX);
			A4Height = (int)Math.Round(11.69f * resY);
			pagePanel.Height = A4Height;

			this.Text = "TextPro: Новый документ";

			foreach (FontFamily font in FontFamily.Families)
			{
				comboBoxFonts.Items.Add(font.Name);
			}
			comboBoxFonts.SelectedItem = richTextBox1.SelectionFont.Name;
			numericUpDownFontSize.Value = (decimal)richTextBox1.SelectionFont.Size;

			RTBResize();
		}


		private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
		{

		}

		private void Form1_Resize(object sender, EventArgs e)
		{
			RTBResize();
		}

		private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
		{
			
			//counterL.Text = richTextBox1.Controls.Count.ToString();
		}

		private void справкаToolStripButton_Click(object sender, EventArgs e)
		{
			
		}

		private void richTextBox1_ContentsResized(object sender, ContentsResizedEventArgs e)
		{
			
			if (e.NewRectangle.Height > A4Height)
			{
				pagePanel.Height = e.NewRectangle.Height + (borderTop + borderBottom) + 100;
				RTBResize();
			}
			//if (e.NewRectangle.Height >= richTextBox1.Height)
			//{
			//	List<string> finalLines = richTextBox1.Lines.ToList();
			//	finalLines.RemoveRange(finalLines.Count - 1, 1);
			//	richTextBox1.Lines = finalLines.ToArray();
			//	//isNormalContent = false;
			//}
			//else
			//{
			//	//isNormalContent = true;
			//}
		}

		private void richTextBox1_TextChanged(object sender, EventArgs e)
		{
			isFileSave = false;
			counterL.Text = "Количество символов: " + richTextBox1.TextLength.ToString();
		}

		private void вставкаToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.Paste();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			
		}

		private void ApplyFont()
		{
			richTextBox1.SelectionFont = fontDialog1.Font;
			richTextBox1.SelectionColor = fontDialog1.Color;
		}

		private void шрифтToolStripMenuItem_Click(object sender, EventArgs e)
		{
			fontDialog1.Font = richTextBox1.SelectionFont;
			fontDialog1.Color = richTextBox1.SelectionColor;
			if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				ApplyFont();
			}
		}

		private void OpenFile()
		{
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				richTextBox1.LoadFile(openFileDialog1.FileName);
				currentFileName = openFileDialog1.FileName;
				this.Text = "TextPro: " + currentFileName;
				isFileSave = true;
			}
		}

		private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFile();
		}

		private void SaveFile()
		{
			if (currentFileName != String.Empty)
			{
				richTextBox1.SaveFile(currentFileName);
				isFileSave = true;
			}
			else
			{
				if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				{
					richTextBox1.SaveFile(saveFileDialog1.FileName);
					currentFileName = saveFileDialog1.FileName;
					this.Text = "TextPro: " + currentFileName;
					isFileSave = true;
				}
			}
		}

		private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFile();
		}

		private void SaveAs()
		{
			if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				richTextBox1.SaveFile(saveFileDialog1.FileName);
				currentFileName = saveFileDialog1.FileName;
				this.Text = "TextPro: " + currentFileName;
				isFileSave = true;
			}
		}

		private void сохранитькакToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveAs();
		}

		private void выходToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (isFileSave)
			{
				e.Cancel = false;
			}
			else
			{
				string message = "Сохранить документ перед закрытием?";
				string caption = "Предупреждение!";
				MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel;
				DialogResult result = MessageBox.Show(message, caption, buttons);
				switch(result)
				{
					case System.Windows.Forms.DialogResult.Yes:
						SaveFile();
						break;
					case System.Windows.Forms.DialogResult.No:
						e.Cancel = false;
						break;
					case System.Windows.Forms.DialogResult.Cancel:
						e.Cancel = true;
						break;
					default:
						break;
				}
			}
		}

		private void отменадействияToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.Undo();
		}

		private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.Cut();
		}

		private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.Copy();
		}

		private void выделитьвсеToolStripMenuItem_Click(object sender, EventArgs e)
		{
			richTextBox1.SelectAll();
		}

		private void NewDoc()
		{
			currentFileName = String.Empty;
			this.Text = "TextPro: Новый документ";
			richTextBox1.Clear();
		}

		private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewDoc();
		}

		private void создатьToolStripButton_Click(object sender, EventArgs e)
		{
			NewDoc();
		}

		private void открытьToolStripButton_Click(object sender, EventArgs e)
		{
			OpenFile();
		}

		private void сохранитьToolStripButton_Click(object sender, EventArgs e)
		{
			SaveFile();
		}

		private void вырезатьToolStripButton_Click(object sender, EventArgs e)
		{
			richTextBox1.Cut();
		}

		private void копироватьToolStripButton_Click(object sender, EventArgs e)
		{
			richTextBox1.Copy();
		}

		private void вставкаToolStripButton_Click(object sender, EventArgs e)
		{
			richTextBox1.Paste();
		}

		private Font GetFont()
		{
			FontStyle fontStyle = ((checkBoxBold.Checked) ? (FontStyle.Bold) : (FontStyle.Regular)) |
				((checkBoxItalic.Checked) ? (FontStyle.Italic) : (FontStyle.Regular)) |
				((checkBoxUnderlined.Checked) ? (FontStyle.Underline) : (FontStyle.Regular)) |
				((checkBoxStrikeout.Checked) ? (FontStyle.Strikeout) : (FontStyle.Regular));
			Font font;
			try
			{
				font = new Font(
					comboBoxFonts.SelectedItem.ToString(),
					(float)numericUpDownFontSize.Value,
					fontStyle,
					GraphicsUnit.Point
					);
			}
			catch (System.Exception ex)
			{
				font = new Font(
					comboBoxFonts.SelectedItem.ToString(),
					(float)numericUpDownFontSize.Value
					);
			}
			return font;
		}

		private void comboBoxFonts_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (comboBoxFonts.SelectedIndex != -1)
			{
				richTextBox1.SelectionFont = GetFont();
			}
		}

		private void numericUpDownFontSize_ValueChanged(object sender, EventArgs e)
		{
			richTextBox1.SelectionFont = GetFont();
		}

		private void checkBoxBold_CheckedChanged(object sender, EventArgs e)
		{
			richTextBox1.SelectionFont = GetFont();
		}

		private void checkBoxItalic_CheckedChanged(object sender, EventArgs e)
		{
			richTextBox1.SelectionFont = GetFont();
		}

		private void checkBoxUnderlined_CheckedChanged(object sender, EventArgs e)
		{
			richTextBox1.SelectionFont = GetFont();
		}

		private void checkBoxStrikeout_CheckedChanged(object sender, EventArgs e)
		{
			richTextBox1.SelectionFont = GetFont();
		}

		private void buttonTextColor_Click(object sender, EventArgs e)
		{
			colorDialogFont.ShowDialog();
			buttonTextColor.BackColor = colorDialogFont.Color;
			richTextBox1.SelectionColor = colorDialogFont.Color;
		}

		private void richTextBox1_SelectionChanged(object sender, EventArgs e)
		{
			buttonTextColor.BackColor = colorDialogFont.Color = richTextBox1.SelectionColor;
			buttonBackrgoundColor.BackColor = colorDialogBack.Color = richTextBox1.SelectionBackColor;
			numericUpDownFontSize.Value = (decimal)richTextBox1.SelectionFont.Size;
			Font font = richTextBox1.SelectionFont;
			comboBoxFonts.SelectedItem = font.Name;
			checkBoxBold.Checked = font.Style.Equals(FontStyle.Bold);
			checkBoxItalic.Checked = font.Style.Equals(FontStyle.Italic);
			checkBoxStrikeout.Checked = font.Style.Equals(FontStyle.Strikeout);
			checkBoxUnderlined.Checked = font.Style.Equals(FontStyle.Underline);
		}

		private void buttonBackrgoundColor_Click(object sender, EventArgs e)
		{
			colorDialogBack.ShowDialog();
			buttonBackrgoundColor.BackColor = colorDialogBack.Color;
			richTextBox1.SelectionBackColor = colorDialogBack.Color;
		}

		private void vScrollBar1_ValueChanged(object sender, EventArgs e)
		{
			rtbPosY = vScrollBar1.Value;
			RTBposition();
		}

		private void buttonPaste_Click(object sender, EventArgs e)
		{
			richTextBox1.Paste();
		}

		private void buttonPasteDate_Click(object sender, EventArgs e)
		{
			AddDateForm addDateForm = new AddDateForm();
			addDateForm.ShowDialog();
			if (addDateForm.DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				String temp = addDateForm.GetString();
				Clipboard.SetText(temp);
				richTextBox1.Paste();
				Clipboard.Clear();
			}
		}

		private void buttonPasteImage_Click(object sender, EventArgs e)
		{
			openFileDialogImage.ShowDialog();
		}

		private void openFileDialogImage_FileOk(object sender, CancelEventArgs e)
		{
			Image img = Image.FromFile(openFileDialogImage.FileName);
			Clipboard.Clear();
			Clipboard.SetImage(img);
			richTextBox1.Paste();
			Clipboard.Clear();
		}
	}
}
